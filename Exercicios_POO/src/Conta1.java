
public class Conta1 {
	
	private int num;
	private double saldo;
	
	public int getNum() {
		return num;
	}
	
	public void setNum(int num) {
		this.num = num;
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	
	
	/**
	 * @category Esse m�todo cria uma conta
	 * @param num
	 * @param saldo
	 */
	public Conta1 (int num, double saldo) {
		this.num = num;
		this.saldo = saldo;
	}
	
	public void sacar(double valor) {
		saldo = saldo - valor;
	}
	
	public void deposito (double valor) {
		saldo = saldo + valor;
	}
	
		
}
