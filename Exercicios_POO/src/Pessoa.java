
public class Pessoa {

	private String nome;
	private int cpf;
	

	public String getNome() {
		return nome;
	}

	public void setNome (String nome) {
		this.nome = nome;
	}
	
	public int getCpf() {
		return cpf;
	}
	
	public void setCpf(int cpf) {
		this.cpf = cpf;
	}	
	
	/**
	 * @category Esse m�todo permite a cria��o de pessoas
	 * @param nome
	 * @param cpf
	 */
	public Pessoa (String nome, int cpf) {
		this.nome = nome;
		this.cpf = cpf;
		
	}
	
	

}
