import java.util.ArrayList;
import java.util.Scanner;


public class Ex5 {

	static ArrayList<Aluno> lista = new ArrayList<Aluno>();
	
	static Scanner in = new Scanner (System.in);
	static float mdturma;
	static int cont;
	
	public static void main(String[] args) {
		
		int op;
		
		do {
			System.out.println("----- MENU ACADEMICO -----");
			System.out.println("1 - Adicionar aluno");
			System.out.println("2 - Excluir aluno");
			System.out.println("3 - Lan�ar a Nota/Editar");
			System.out.println("4 - Listar todos os alunos");
			System.out.println("5 - Pesquisar um aluno");
			System.out.println("6 - Sair do programa");
			System.out.println("Digite sua op��o: ");
			op = in.nextInt();
			
			switch (op) {
			case 1: InserirAluno(); break;
			case 2: ExcluirAluno();	break;
			case 3: Lancar(); break;
			case 4: Listar(); break;
			case 5: Pesquisar(); break;
			case 6: break;
			
			}
		}while(op!=6);
		System.out.println("----- Menu Academico finalizado -----");
		System.out.println("A m�dia da turma � "+mdturma/cont);
		System.out.println("");
		System.out.println("     FIM DO PROGRAMA     ");

	}

	public static void InserirAluno() {
		System.out.println("----- Adicionando aluno -----");
		System.out.println("Digite o nome do aluno: ");
		String nome = in.next();
		System.out.println("Digite a matricula do aluno: ");
		int mat = in.nextInt();
		lista.add(new Aluno(mat,nome));
		System.out.println("Aluno criado com sucesso");
	}
	
	public static void ExcluirAluno() {
		System.out.println("----- Excluindo aluno -----");
		System.out.println("Digite a matricula do aluno que deseja excluir: ");
		int mat = in.nextInt();
		for (Aluno i : lista) {
			if (i.getMat()==mat) {
				System.out.println("O aluno de matricula " +mat+" foi excluido com sucesso");
				lista.remove(i);
				break;
			}else {
				System.out.println("N�o existe nenhum aluno com essa matricula! ");
				break;
			}
		}
	}
	
	public static void Lancar() {
		System.out.println("----- Lan�ar as notas -----");
		System.out.println("Digite a matricula do aluno: ");
		int mat = in.nextInt();
		System.out.println("Digite a nota da AV1");
		float av1 = in.nextFloat();
		System.out.println("Digite a nota da AV2");
		float av2 = in.nextFloat();
			for (Aluno i:lista) {
				if(i.getMat()==mat) {
					i.setAV1(av1);
					i.setAV2(av2);
					i.setMedia(av1, av2);
					mdturma += i.getMedia();
					if (i.getMedia()>=7) {
						i.Aprovado();
					}else {
						i.Reprovado();
					}
				}
			}
		cont++;
	}
	
	public static void Listar() {
		System.out.println("----- Listando todos os alunos -----");
		if(lista.isEmpty()==false) {
			for (Aluno i: lista) {
				System.out.println("Nome: "+i.getNome());
				System.out.println("Matricula: "+i.getMat());
				System.out.println("AV1 = "+i.getAV1());
				System.out.println("AV2 = "+i.getAV2());
				System.out.println("M�dia = "+i.getMedia());
				System.out.println("Situa��o: "+i.getSit());
			}
		} else {
			System.out.println("Lista vazia! Nenhum aluno foi cadastrado");
		}
	}
	
	public static void Pesquisar() {
		System.out.println("----- Pesquisa -----");
		System.out.println("Digite a matricula do aluno para pesquisar ");
		int mat = in.nextInt();
			for(Aluno i: lista) {
				if (i.getMat()==mat) {
					System.out.println("Nome:"+i.getNome());
					System.out.println("Matricula: "+i.getMat());
					System.out.println("AV1 = "+i.getAV1());
					System.out.println("AV2 = "+i.getAV2());
					System.out.println("M�dia = "+i.getMedia());
					System.out.println("Situa��o: "+i.getSit());
					break;
				}else {
					System.out.println("N�o existe nenhum aluno com essa matricula!");
				}
			}
	}
	
}
