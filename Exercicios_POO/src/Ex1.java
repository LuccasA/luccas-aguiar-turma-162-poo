import java.util.Scanner;

public class Ex1 {
	
	static final int Tamanho = 10;
	static int index = 0;
	
	static Pessoa[] lista = new Pessoa[Tamanho]; 
	
	static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {
		int op;
		do {
			System.out.println("*** Menu Principal ***");
			System.out.println("1 - Adicionar nova pessoa");
			System.out.println("2 - Listar todas as pessoas");
			System.out.println("3 - Sair");
			System.out.println("Escolha uma das op��es!!");
			op = in.nextInt();
			
			switch (op){
				case 1: incluirPessoa(); break;
				case 2: listarPessoas(); break;
				case 3: break;
			}			
		} while (op!=3);	
	}
	
	public static void incluirPessoa() {
		String nome;
		int cpf;
		
		System.out.println("Insira o nome: ");
		nome = in.next();
		System.out.println("Insira o cpf: ");
		cpf = in.nextInt();
		lista[index++] = new Pessoa(nome,cpf);
		System.out.println("Perfil concluido");
		
	}
	
	public static void listarPessoas() {
		int total = 0;
		System.out.println("Todos os perfis cadastrados");
		for (int i = 0; i < lista.length-1; i++) {
			if (lista[i] != null) {
				System.out.println(lista[i].getNome()
									+"......."+
									lista[i].getCpf());
			}else {
				break;
			}
			total++;
		}
		System.out.println("Total de pessoas cadastradas: "+total);
	}

}
