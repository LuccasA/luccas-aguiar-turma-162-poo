import java.util.Scanner;

public class Ex3 {

	static final int tamanho = 50;
	static int index = 0;
	static int total = 0;
	
	static Scanner in = new Scanner (System.in);
	
	static Candidato[] lista = new Candidato[tamanho];
	
	public static void main(String[] args) {
		int op;
		
		do {
			System.out.println("Sistema de vota��o! ");
			System.out.println("1 - Criar um candidato ");
			System.out.println("2 - Votar em um candidato ");
			System.out.println("3 - Verificar os percentuais ");
			System.out.println("4 - Para concluir a vota��o");
			System.out.println("Digite a op��o desejada: ");
			String a = new String  ("A");
			System.out.println(a);
			String b = a;
			String c = "A";
			String d = "A";
			System.out.println(a.equals(b));
			System.out.println(c==d);
			op = in.nextInt();
			
			
			switch (op) {
				case 1: CriarCand(); break;
				case 2: Votar(); break;
				case 3: Percentual(); break;
				case 4: break;
			}			
		}while (op!=4);
		System.out.println("A vota��o foi encerrada");
			Resultado();
		
	}
	
	public static void CriarCand() {
		
		
		System.out.println("Digite o nome do candidato ");
		String nome = in.next();
		System.out.println("Digite o n�mero do candidato ");
		int num = in.nextInt();
		lista[index++] = new Candidato(num, nome);
		System.out.println("Candidato cadastrado com sucesso! ");
		

	}
	
	public static void Votar() {
		System.out.println("Digite o n�mero do candidato ");
		int num = in.nextInt();
			for (int i = 0; i<lista.length-1;i++) {
				if(lista[i] != null) {
					if(num == lista[i].getNum()) {
						System.out.println("Candidato: "+lista[i].getNome()
														+" de n�mero "+lista[i].getNum()+
														" recebeu seu voto:");
						lista[i].setVotos();
						
						}
					}
					
				}
			total++;
			}
	
	
	public static void Percentual() {
		Candidato [] teste = new Candidato [tamanho];
		
		System.out.println("Parciais dos candidatos! ");
			for (int i=0;i <lista.length-1;i++) {
				if (lista[i] !=null) {
					for (int j = 0;j<i;j++) {
						if (lista[j]!= null) {
							if (lista[j].getVotos() < lista[i].getVotos()) {
								teste[i] = lista[i];
								lista[i] = lista[j];
								lista [j] = teste[i];
							}
						}
					}
				}
			}
		
			for (int i = 0; i < lista.length-1;i++) {
				if (lista[i]!= null) {
					
					System.out.println("O candidato "+lista[i].getNome()+
										" tem como total de votos  "+lista[i].getVotos()+ " e percentual de "+ (lista[i].getVotos()*100)/total + "%");
				
				}
			}
		}	
	
	
	public static void Resultado() {
		
		Candidato [] teste = new Candidato [tamanho];
		
		for (int i=0;i <lista.length-1;i++) {
			if (lista[i] !=null) {
				for (int j = 0;j<i;j++) {
					if (lista[j]!= null) {
						if (lista[j].getVotos() < lista[i].getVotos()) {
							teste[i] = lista[i];
							lista[i] = lista[j];
							lista [j] = teste[i];
						}
					}
				}
			}
		}
		
		for (int i = 0; i < lista.length-1;i++) {
			if (lista[i]!= null) {
				
					System.out.println("O candidato "+lista[i].getNome()+
										" possui a quantidade de "+lista[i].getVotos()+ " votos e percentual de "+ (lista[i].getVotos()*100)/total + "%");
				
			}
		}
	
	}
}
