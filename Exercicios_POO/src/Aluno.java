
public class Aluno {

	private String nome;
	private int mat;
	private float av1, av2;
	private String situacao;
	private float media;
	
	
	
	public int getMat() {
		return mat;
	}
	
	public void setMat(int mat) {
		this.mat = mat;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public float getAV1() {
		return av1;
	}
	
	public void setAV1(float av1) {
		this.av1 = av1;
	}
	
	public float getAV2() {
		return av2;
	}
	
	public void setAV2(float av2) {
		this.av2 = av2;
	}
	
	public String getSit() {
		return situacao;
	}
	
	public void setSit(String situacao) {
		this.situacao = situacao;
	}
	
	public float getMedia() {
		return media;
	}
	
	public void setMedia(float av1, float av2) {
		this.media = (av1+av2)/2;
	}
	
	public Aluno(int m, String n) {
		
		mat = m;
		nome = n;
		av1 = 0;
		av2 = 0;
		situacao = "Esse aluno ainda n�o teve sua nota lan�ada";
	}
	
	public void Aprovado() {
		this.situacao = "Aprovado";
	}
	
	public void Reprovado() {
		this.situacao = "Reprovado";
	}
	
	
}
