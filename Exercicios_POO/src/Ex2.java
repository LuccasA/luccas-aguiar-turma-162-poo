import java.util.Scanner;

public class Ex2 {

	static final int tamanho = 10;
	static int index = 0;
	
	static Conta1[] lista = new Conta1[tamanho];
	
	static Scanner in = new Scanner (System.in);
	
	public static void main(String[] args) {
		int op;
		do {
			System.out.println("*****Menu Principal*****");
			System.out.println("------------------------");
			System.out.println("1 - Adicionar conta: ");
			System.out.println("2 - Excluir conta: ");
			System.out.println("3 - Consultar Saldo: ");
			System.out.println("4 - Efetuar saque: ");
			System.out.println("5 - Efetuar deposito: ");
			System.out.println("6 - Pesquisar conta: ");
			System.out.println("7 - Efetuar transfer�ncia entre contas: ");
			System.out.println("Digite sua op��o: ");
			op = in.nextInt();
				switch (op) {
				case 1: incluirConta(); break;
				case 2: excluirConta();	break;
				case 3: consultaSaldo(); break;
				case 4: efetuarSaque(); break;
				case 5: efetuarDeposito(); break;
				case 6: pesquisaConta(); break;
				case 7: transferencia(); break;
				case 8: break;
				}
		} while(op!=8);
	}
	
	public static void incluirConta() {
		int num;
		double saldo;
			System.out.println("-------- Criando nova conta --------");
			System.out.println("Insira o numero da sua nova conta: ");
			num = in.nextInt();
			System.out.println("Insira o saldo da sua nova conta: ");
			saldo = in.nextDouble();
			lista[index++] = new Conta1(num, saldo);
			System.out.println("Sua conta foi criada com sucesso! ");
		}
	
	public static void excluirConta() {
		int num;
		System.out.println("-------- Excluindo conta --------");
		System.out.println("Insira o numero da conta que deseja excluir: ");
		num=in.nextInt();
		
		for(int i=0;i < lista.length-1;i++) {
			if (lista[i]!=null) {
					if (num == lista[i].getNum()) {
						System.out.println("A conta: "+lista[i].getNum()+" foi excluida");
						lista[i] = null;
						break;
					}
				}else {
					System.out.println("Essa conta n�o foi criada!");
					break;
					}
			}
		}
	public static void consultaSaldo() {
		int num=0;
		System.out.println("-------- Consultando saldo --------");
		System.out.println("Digite o numero da conta: ");
		num = in.nextInt();
		for (int i = 0; i < lista.length-1; i++) {
			if (lista[i]!=null) {
					if (num == lista[i].getNum()) {
						System.out.println("Valor do saldo da conta �: "+lista[i].getSaldo());
						break;
					}
				}
			}
	}
	
	public static void efetuarSaque() {
		int num;
		
		System.out.println("-------- Efetuando Saque --------");
		System.out.println("Digite a conta para realizar o saque: ");
		num = in.nextInt();
		System.out.println("Digite o valor do saque: ");
		double valor = in.nextDouble();
		
			for (int i = 0; i < lista.length-1; i++) {
				if (lista[i]!=null) {
					if(num == lista[i].getNum()) {
						lista[i].sacar(valor);
						System.out.println("Saque realizado com sucesso!!!");
						break;
					}
				}
			}
	}
	
	public static void efetuarDeposito() {
		
		
		System.out.println("-------- Efetuando deposito --------");
		System.out.println("Digite a conta para realizar o deposito: ");
		int num = in.nextInt();
		System.out.println("Digite o valor que deseja depositar: ");
		double valor = in.nextDouble();
		
			for (int i = 0; i<lista.length-1; i++) {
				if (lista[i]!=null) {
					if(num == lista[i].getNum()) {
						lista[i].deposito(valor);
						System.out.println("Deposito realizado com sucesso!");
						break;
					}
				}
			}
	}
	
	public static void pesquisaConta() {
		
		System.out.println("-------- Pesquisando --------");
		System.out.println("Digite o n�mero da conta: ");
		int num = in.nextInt();
			for (int i = 0; i<lista.length-1; i++) {
				if (lista[i]!=null) {
					if (num == lista[i].getNum()) {
						System.out.println("A conta "+lista[i].getNum()+
													" possui de saldo "+lista[i].getSaldo());
					}
				}
			}
	}
	
	public static void transferencia() {
		System.out.println("-------- Transferencia --------");
		System.out.println("Digite sua conta: ");
		int num = in.nextInt();
		System.out.println("Digite o valor que voce deseja transferir: ");
		double valor = in.nextDouble();
		
			for (int i = 0; i< lista.length-1;i++) {
				if (lista[i]!=null) {
					if(num==lista[i].getNum()) {
						lista[i].sacar(valor);
						break;
						
					}
				}
			}
		System.out.println("Digite a conta destino: ");
		num = in.nextInt();
			for (int i = 0; i<lista.length-1; i++) {
				if (lista[i]!=null) {
					if(num ==lista[i].getNum()) {
						lista[i].deposito(valor);
						System.out.println("Deposito realizado com sucesso!!");
						break;
					}
				}
			}
	}
}


