
public class Candidato {

	private int num;
	private String nome;
	private int votos;
	

	public int getNum() {
		return num;
	}
	
	public void setNum(int num) {
		this.num = num;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getVotos() {
		return votos;
	}
	
	public void setVotos() {
		this.votos++;
	}
	
	
	/**
	 * @category Construtor padr�o
	 * @param num
	 * @param nome
	 * @param votos
	 */
	public Candidato(int num,String nome) {
		this.num = num;
		this.nome = nome;
		this.votos = 0;
	}
	
}
