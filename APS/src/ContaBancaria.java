
public abstract class ContaBancaria {

	protected long numeroConta;
	protected double saldo;
	abstract public void sacar(double valor);
	abstract public void depositar(double valor);
	
	
	public ContaBancaria(long numeroConta, double saldo) {
		this.numeroConta = numeroConta;
		this.saldo = saldo;
	}
	
	public long getNumCont(){
		return numeroConta;
	}
	
	public void setNumCont(long numeroConta) {
		this.numeroConta = numeroConta;
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(double saldo) {
		this.saldo= saldo;
	}
	
	
}
