
public class ContaPoupanca extends ContaBancaria {
	
	private double limite;
	
	
	public ContaPoupanca(long numeroConta, double saldo) {
		super(numeroConta, saldo);
		limite = 1000;
	}
	
	
	public double getLimite() {
		return limite;
	}

	public void setLimite(double limite) {
		this.limite = limite;
	}
	
	@Override
	public void sacar(double valor) {
		if (valor <= limite) {
			saldo -= valor;
		}else {
			System.out.println("Voc� est� estourando o limite de saque da conta.");
		}
	}
	
	@Override
	public void depositar(double valor) {
		saldo += valor;
	}
	
	
}
