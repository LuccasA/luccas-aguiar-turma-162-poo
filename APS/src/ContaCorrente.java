
public class ContaCorrente extends ContaBancaria {
	
	private double taxaDeOperacao;
	
	
	public ContaCorrente (long numeroConta, double saldo) {
		super(numeroConta,saldo);
		taxaDeOperacao = 0.5;
	}

	public double getTaxadeoperacao() {
		return taxaDeOperacao;
	}
	
	public void setTaxadeoperacao(double taxaDeOperacao) {
		this.taxaDeOperacao = taxaDeOperacao;
	}

	@Override
	public void sacar(double valor) {
		saldo -= valor;
	}
	
	@Override
	public void depositar(double valor) {
		 saldo += valor;
	}
	
	
	
}
